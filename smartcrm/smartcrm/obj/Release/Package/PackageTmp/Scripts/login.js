﻿$(function () {

    $("#Password").keydown(function (e) {      
        if (e.which == 13) {
            $('#btnLogin').click();
            return false;
        }
    });

    $("#btnLogin").click(function () {
        if (checkRequired("loginForm", "errorMessage")) {
            $.ajax({
                url: "/Account/Login",
                type: 'POST',
                cache: false,
                data: { UserId: $("#UserId").val(), Password: $("#Password").val() },
                success: function (result) {
                    if (result.code == 0) {
                        location.href = "/MySpace"
                    }
                    else {
                        $(".errorSpan").text('帐号或密码错误');
                    }
                },
                error: function (e) {
                    $(".errorSpan").text('系统错误，请联系管理员');
                }
            });
        }
    });

});