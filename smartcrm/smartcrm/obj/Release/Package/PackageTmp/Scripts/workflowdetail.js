﻿
var isAdd = true;

function EditStep(WorkFlowStepNo) {  
    isAdd = false;

    clearForm(isAdd);

    $.ajax({
        url: "/System/GetWorkFlowStep?WorkFlowNo=" + $("#WorkFlowNo").val() + "&WorkFlowStepNo" + WorkFlowStepNo,
        type: 'GET',
        cache: false,
        data: { WorkFlowStepNo: WorkFlowStepNo },
        success: function (result) {
            if (result.code == 0) {
                $("#WorkFlowStepNo").val(result.data.WorkFlowStepNo);
                $("#WorkFlowStepName").val(result.data.WorkFlowStepName);
                $("#SN").val(result.data.SN);
                $("#StatusNo").val(result.data.StatusNo);
                $("#aUser").empty();
                $("#bUser").empty();
             
                for (var i = 0; i < result.data.Users.length; i++) {
                    if (result.data.Users[i].AdditionalValue == "0") {
                        $("#aUser").append("<option value='" + result.data.Users[i].OptionValue + "'>" + result.data.Users[i].OptionText + "</option>");
                    }
                    else {
                        $("#bUser").append("<option value='" + result.data.Users[i].OptionValue + "'>" + result.data.Users[i].OptionText + "</option>");
                    }
                }

                $("#aFollowUp").empty();
                $("#bFollowUp").empty();
                for (var i = 0; i < result.data.FollowUps.length; i++) {
                    if (result.data.FollowUps[i].AdditionalValue == "0") {
                        $("#aFollowUp").append("<option value='" + result.data.FollowUps[i].OptionValue + "'>" + result.data.FollowUps[i].OptionText + "</option>");
                    }
                    else {
                        $("#bFollowUp").append("<option value='" + result.data.FollowUps[i].OptionValue + "'>" + result.data.FollowUps[i].OptionText + "</option>");
                    }
                }

                showStep();
            }
            else {
                $("#errorMessage").text(result.message);
            }

        },
        error: function (e) {
            $("#errorMessage").text("查找信息失败");
        }
    });

    return false;
}

function AddStep() {
    isAdd = true;

    clearForm(isAdd);
   
    $.ajax({
        url: "/System/GetWorkFlowStep?WorkFlowNo=" + $("#WorkFlowNo").val() ,
        type: 'GET',
        cache: false,
        data: {},
        success: function (result) { 
            if (result.code == 0) {
                $("#WorkFlowStepNo").val(result.data.WorkFlowStepNo);
                $("#WorkFlowStepName").val(result.data.WorkFlowStepName);

                $("#aUser").empty();
                $("#bUser").empty();
                for (var i = 0; i < result.data.Users.length; i++) {
                    if (result.data.Users[i].AdditionalValue == "0") {
                        $("#aUser").append("<option value='" + result.data.Users[i].OptionValue + "'>" + result.data.Users[i].OptionText + "</option>");
                    }
                    else {
                        $("#aUser").append("<option value='" + result.data.Users[i].OptionValue + "'>" + result.data.Users[i].OptionText + "</option>");
                    }
                }

                $("#aFollowUp").empty();
                $("#bFollowUp").empty();
                for (var i = 0; i < result.data.FollowUps.length; i++) {
                    if (result.data.FollowUps[i].AdditionalValue == "0") {
                        $("#aFollowUp").append("<option value='" + result.data.FollowUps[i].OptionValue + "'>" + result.data.FollowUps[i].OptionText + "</option>");
                    }
                    else {
                        $("#bFollowUp").append("<option value='" + result.data.FollowUps[i].OptionValue + "'>" + result.data.FollowUps[i].OptionText + "</option>");
                    }
                }

                showStep();
            }
            else {
                $("#errorMessage").text(result.message);
            }

        },
        error: function (e) {
            $("#errorMessage").text("查找信息失败");
        }
    });

    return false;
}



function DeleteStep(WorkFlowStepNo) {
    if (!confirm('确定删除吗？')) {
        return;
    }
    $.ajax({
        url: "/System/DeleteWorkFlowStep",
        type: 'POST',
        cache: false,
        data: { WorkFlowStepNo: WorkFlowStepNo },
        success: function (result) {
            if (result.code == 0) {
                location.href = "/System/WorkFlowDetail?WorkFlowNo=" + $("#WorkFlowNo").val();
            }
            else {
                alert(result.message);
            }

        },
        error: function (e) {
            alert('删除失败');
        }
    });

   return false;
}


$(function () { 
    $("#canvas").attr("width", $("#canvas").parent().width());
    $('#divStep').dialog({
        width: 600,
        autoOpen: false,
        modal: true,
        buttons: {
        }
    });

    $('#btnNew').click(function () {
        AddStep();
    });

    $('#btnCancel').click(function () {
        $('#divStep').dialog('close');
    });

    $("#btnAddUser").click(function () {
        MoveOptions("aUser", "bUser");
    });

    $("#btnRemoveUser").click(function () {
        MoveOptions("bUser", "aUser");
    });

    $("#btnAddStep").click(function () {
        MoveOptions("aFollowUp", "bFollowUp");
    });

    $("#btnRemoveStep").click(function () {
        MoveOptions("bFollowUp", "aFollowUp");
    });


    $("#btnSave").click(function () {

        if (checkRequired('divStep', 'errorMessage') == false) {
            return;
        }

        if (!confirm("确定要保存吗？")) {
            return;
        }
        var users = [];
        var followups = [];
        $("#bUser option").each(function () {
            users.push(this.value);
        });
        $("#bFollowUp option").each(function () {
            followups.push(this.value);
        });

        if (isAdd) {
            $.ajax({
                url: "/System/AddWorkFlowStep",
                type: 'POST',
                cache: false,
                data: { Users: JSON.stringify(users),
                    FollowUps: JSON.stringify(followups),
                    WorkFlowStepNo: $("#WorkFlowStepNo").val(),
                    WorkFlowStepName: $("#WorkFlowStepName").val(),
                    WorkFlowNo: $("#WorkFlowNo").val(),
                    StatusNo: $("#StatusNo").val(),
                    SN: $("#SN").val()
                },
                success: function (result) {
                    if (result.code == 0) {
                        closeStep();
                        location.href = "/System/WorkFlowDetail?WorkFlowNo=" + $("#WorkFlowNo").val();
                    }
                    else {
                        $("#errorMessage").text(result.message);
                    }
                },
                error: function (e) {
                    $("#errorMessage").text("保存失败.");
                }
            });
        }
        else {
            $.ajax({
                url: "/System/UpdateWorkFlowStep",
                type: 'POST',
                cache: false,
                data: { Users: JSON.stringify(users),
                    FollowUps: JSON.stringify(followups),
                    WorkFlowStepNo: $("#WorkFlowStepNo").val(),
                    WorkFlowStepName: $("#WorkFlowStepName").val(),
                    WorkFlowNo: $("#WorkFlowNo").val(),
                    StatusNo: $("#StatusNo").val(),
                    SN: $("#SN").val()
                },
                success: function (result) {
                    if (result.code == 0) {
                        closeStep();
                        location.href = "/System/WorkFlowDetail?WorkFlowNo=" + $("#WorkFlowNo").val();
                    }
                    else {
                        $("#errorMessage").text(result.message);
                    }
                },
                error: function (e) {
                    $("#errorMessage").text("保存失败.");
                }
            });
        }

    });

    // Get data and draw the canvas
    $.ajax({
        url: "/System/GetWorkFlowDetail",
        type: 'POST',
        cache: false,
        data: { WorkFlowNo: $("#WorkFlowNo").val() },
        success: function (result) {
            if (result.code == 0) {
                DrawCanvas(result.data);
            }
            else {
                alert(result.message);
                location.herf = "/System/WorkFlow";
            }
        },
        error: function (e) {
            alert("获取数据失败，请稍后重试");
        }
    });

});

function DrawCanvas(data) {
    var canvas = document.getElementById("canvas");
    if (canvas == null || data == null) {
        return;
    }
    var context = canvas.getContext("2d");
    var ix = 5;
    var iy = 0;
    var iw = 74;
    var ih = 100;
    var iiw = 34;
    var iih = 100;
    iy = iih;

    for (var i in data.Steps) {
        context.beginPath();
        data.Steps[i].xs = ix;
        data.Steps[i].xe = data.Steps[i].xs + iw;
        data.Steps[i].ys = iy;
        data.Steps[i].ye = data.Steps[i].ys + ih;
        if (data.Steps[i].IsSystemReserved) {
            context.fillStyle = "#4BAFBE"; //"#0000FF";
            data.Steps[i].width = iw;
        }
        else {
            context.fillStyle = "#4BAFBE";
            data.Steps[i].width = iw;
        }
        data.Steps[i].height = ih;
        data.Steps[i].xc = ix + iw / 2;
        data.Steps[i].yc = iy + ih / 2;
       
        context.fillRect(data.Steps[i].xs, data.Steps[i].ys, data.Steps[i].width, data.Steps[i].height);
        context.fillStyle = "rgb(255,255,255)";
        context.font = "normal bold 14px Arial";
        context.textBaseline = 'top';
        var length = context.measureText(data.Steps[i].WorkFlowStepName).width;

        if (length > 140) {
            context.font = "normal bold 12px Arial";           
            var j = parseInt(data.Steps[i].WorkFlowStepName.length / 3);         
            var part1 = data.Steps[i].WorkFlowStepName.substr(0, j);
            var part2 = data.Steps[i].WorkFlowStepName.substr(j, j);
            var part3 = data.Steps[i].WorkFlowStepName.substr(2*j, data.Steps[i].WorkFlowStepName.length - 2*j);          
            context.fillText(part1, data.Steps[i].xs + 5, data.Steps[i].ys + 25);
            context.fillText(part2, data.Steps[i].xs + 5, data.Steps[i].ys + 45);
            context.fillText(part3, data.Steps[i].xs + 5, data.Steps[i].ys + 65);
             
         }
         else if (length > 70) {
            var j = parseInt(data.Steps[i].WorkFlowStepName.length / 2);           
            var part1 = data.Steps[i].WorkFlowStepName.substr(0, j);
            var part2 = data.Steps[i].WorkFlowStepName.substr(j, data.Steps[i].WorkFlowStepName.length - j);           
            context.fillText(part1, data.Steps[i].xs + 5, data.Steps[i].ys + 30);
            context.fillText(part2, data.Steps[i].xs + 5, data.Steps[i].ys + 50);
         }
         else {
            context.fillText(data.Steps[i].WorkFlowStepName, data.Steps[i].xs + (70-length)/2, data.Steps[i].ys + 40);
         }        
        ix = ix+data.Steps[i].width +iiw;
        iy = iy;
    }

    for (var i = 0; i < data.Steps.length;i++ ) {
        data.Steps[i].next = [];
        data.Steps[i].previous  =[];
        data.Steps[i].nextspan  =[];
        data.Steps[i].previousspan  =[];
        for (var j in data.Steps[i].FollowUps) {
            
            var index = GetIndexByStepNo(data,data.Steps[i].FollowUps[j].OptionValue);
           
            if (index == i + 1) {
                data.Steps[i].next.push({ xs: data.Steps[i].xe, xe: data.Steps[index].xs, ys: 150, ye: 150 });
            }
            else if (index == i-1){
                data.Steps[i].previous.push({xs:data.Steps[i].xs, xe:data.Steps[index].xe,ys:150,ye:150});
            }
            else if (index > i+1){
                 data.Steps[i].nextspan.push({xs:data.Steps[i].xc+10, xe:data.Steps[index].xc-10});
            }
            else{
                 data.Steps[i].previousspan.push({xs:data.Steps[i].xc-10, xe:data.Steps[index].xc+10});
            }
        }
    }

    var color = [];
    var colorIndex = 0;
    color.push('rgb(0,0,255)');
    color.push('rgb(0,175,255)');
    color.push('rgb(0,140,140)'); 

    var topinterval = 25;
    var bottominterval = 25;   
    context.lineWidth = 1;
    for (var i = 0; i < data.Steps.length;i++ ) {       
        if (i < data.Steps.length -1 &&
            data.Steps[i + 1].previous.length == 1 &&
            data.Steps[i].next.length > 0) {            
            data.Steps[i].next[0].ys = 140;
            data.Steps[i].next[0].ye =  data.Steps[i].next[0].ys;
            data.Steps[i + 1].previous[0].ys = 160;
            data.Steps[i + 1].previous[0].ye = data.Steps[i + 1].previous[0].ys;
        }

        if (data.Steps[i].next.length > 0) {
            context.beginPath();
            context.strokeStyle = color[colorIndex % 3];            
            colorIndex++;       
            context.moveTo(data.Steps[i].next[0].xs, data.Steps[i].next[0].ys);
            context.lineTo(data.Steps[i].next[0].xe, data.Steps[i].next[0].ye);
            context.stroke();
            context.moveTo(data.Steps[i].next[0].xe, data.Steps[i].next[0].ye);
            context.lineTo(data.Steps[i].next[0].xe - 12, data.Steps[i].next[0].ye - 6);
            context.stroke();
            context.moveTo(data.Steps[i].next[0].xe, data.Steps[i].next[0].ye);
            context.lineTo(data.Steps[i].next[0].xe - 12, data.Steps[i].next[0].ye + 6);
            context.stroke();
        }


        if (i < data.Steps.length - 1 && data.Steps[i + 1].previous.length > 0) {
            context.beginPath();
            context.strokeStyle = color[colorIndex % 3];
            colorIndex++;           
            context.moveTo(data.Steps[i + 1].previous[0].xs, data.Steps[i + 1].previous[0].ys);
            context.lineTo(data.Steps[i + 1].previous[0].xe, data.Steps[i + 1].previous[0].ye);
            context.stroke();          
            context.moveTo(data.Steps[i + 1].previous[0].xe, data.Steps[i + 1].previous[0].ye);
            context.lineTo(data.Steps[i + 1].previous[0].xe + 12, data.Steps[i + 1].previous[0].ye - 6);
            context.stroke();         
            context.moveTo(data.Steps[i + 1].previous[0].xe, data.Steps[i + 1].previous[0].ye);
            context.lineTo(data.Steps[i + 1].previous[0].xe + 12, data.Steps[i + 1].previous[0].ye + 6);
            context.stroke();          
        }
      
        if (data.Steps[i].nextspan.length > 0) {

            for (var k in data.Steps[i].nextspan) {
                context.beginPath();
                context.strokeStyle = color[colorIndex % 3]; 
                colorIndex++;  
                context.moveTo(data.Steps[i].xc, 100);
                context.lineTo(data.Steps[i].xc, 100 - topinterval);
                context.lineTo(data.Steps[i].nextspan[k].xe, 100 - topinterval);
                context.lineTo(data.Steps[i].nextspan[k].xe, 100);
                context.lineTo(data.Steps[i].nextspan[k].xe - 6, 100 - 12);
                context.moveTo(data.Steps[i].nextspan[k].xe, 100);
                context.lineTo(data.Steps[i].nextspan[k].xe + 6, 100 - 12);
                context.stroke();
                topinterval = topinterval + 15;
            }
        }

        if (data.Steps[i].previousspan.length > 0) {
            for (var k in data.Steps[i].previousspan) {
                context.beginPath();
                context.strokeStyle = color[colorIndex % 3];
                colorIndex++;  
                context.moveTo(data.Steps[i].previousspan[k].xs, 200);
                context.lineTo(data.Steps[i].previousspan[k].xs, 200 + bottominterval);
                context.lineTo(data.Steps[i].previousspan[k].xe, 200 + bottominterval);
                context.lineTo(data.Steps[i].previousspan[k].xe, 200);
                context.lineTo(data.Steps[i].previousspan[k].xe - 6, 200 + 12);
                context.moveTo(data.Steps[i].previousspan[k].xe, 200);
                context.lineTo(data.Steps[i].previousspan[k].xe + 6, 200 + 12);
                context.stroke();
                bottominterval = bottominterval + 15;                
            }
        }        
    }   
 
}

function GetIndexByStepNo(data, step) {      
    for (var i = 0; i < data.Steps.length; i++) {
        if (data.Steps[i].WorkFlowStepNo == step) {
            return i;
        }
    }
  
    return 0;
}

function showStep() {
    $('#divStep').dialog('open');
}

function closeStep() {
    $('#divStep').dialog('close');
}


function clearForm(isAdd) {
    $("input").removeClass("redBorder");
    $("input[type=text]").val("");
    $("select").val("");
    $(".errorSpan").html("");
}

function MoveOptions(sourceId, destinationId) {
    var x = $("#" + sourceId+" option");
    var aDepartmentList = $("#" + sourceId);
    var departmentList = $("#"+destinationId);
    var msgs = "";
    x.each(function () {
        if (this.selected) {
            departmentList.prepend(this.outerHTML);
            $("#"+ sourceId + " option[value='" + this.value + "']").remove();
        }
    });
}