﻿var orderNo;
var allCustomer = [];

$(function () {
    SetColor();

    $("#sc").find("option").each(function (i) {
        allCustomer.push({ label: this.text, value: this.value });
    });

    try
    {
        $('#txtCustomer').autocomplete({
            source: allCustomer,
            select: function (a, b, c) {
                $("#sc").val(b.item.value);
                $('#divSearchCustomer').dialog('close');
            }
        });
    }
    catch(e){
    }

    $("#PaymentAmount").inputmask({ 'mask': "9{0,9}.{0,1}9{0,2}", greedy: false });
        
    $('#divDetail').dialog({
        width: 950,
        autoOpen: false,
        modal: true,
        buttons: {
        }
    });

    $('#divPayment').dialog({
        width: 600,
        autoOpen: false,
        modal: true,
        buttons: {
        }
    });

    $('#divSearchCustomer').dialog({
        width: 300,
        autoOpen: false,
        modal: true,
        position: ['', 80],
        buttons: {
        }
    });
    

    $("#sb").attr("readonly", "readonly");
    $("#se").attr("readonly", "readonly");
    $("#sb").datepicker({ dateFormat: 'yy-mm-dd' });
    $("#se").datepicker({ dateFormat: 'yy-mm-dd' });

    $("#sb,#se").dblclick(function () {
        $(this).val("");
    });

});

function ShowDetail(id) {

    $.ajax({
        url: "/Order/GetDetail",
        type: 'POST',
        cache: false,
        data: { OrderNo: id },
        success: function (result) {
            if (result.code == 0) {
                $("#dStatus").html(result.data.StatusDescription);
                $("#dPaymentStatus").removeClass("OrderStatusUnPaid");
                $("#dPaymentStatus").removeClass("OrderStatusPaid");
                $("#dPaymentStatus").addClass(result.data.Css);
                $("#dPaymentStatus").html(result.data.PaymentStatusDescription);
                $("#dOrderNo").html(result.data.OrderNo);
                $("#dOrderName").html(result.data.OrderName);
                $("#dCustomer").html(result.data.CustomerShortName);
                $("#dContact").html(result.data.Contact);
                $("#dAmount").html(result.data.Amount);
                $("#dFactoryAmount").html(result.data.FactoryAmount);
                $("#dNumber").html(result.data.Number);
                $("#dExpectDeliveryDate").html(result.data.ExpectDeliveryDateString);
                $("#dCreatedTime").html(result.data.CreatedTimeString);
                $("#dTransactionDate").html(result.data.TransactionDateString);
                $("#dCreator").html(result.data.Creator);
                $("#dPaid").html(result.data.Paid);
                $("#dDetail").html(result.data.Detail);
                $("#dHistory").html(result.data.History);
                $("#dPayment").html(result.data.Payment);
                $("#dMemo").html(result.data.Memo);
                $("#dQueryURL").html("");
                var a = $("<a target='_blank' href='" + result.data.QueryURL + "'>" + result.data.QueryURL + "</a>").appendTo($("#dQueryURL"));
                GenerateWorkFlowSteps(result.data.steps);

                if (result.data.OrderMemos.length > 0) {
                    $("#MemoPanel").show();
                }
                else {
                    $("#MemoPanel").hide();
                }
                $("#dOrderMemo").html("");
                for (var r in result.data.OrderMemos) {
                    $("<tr><td>" + result.data.OrderMemos[r].Addition1 + "</td><td>" + result.data.OrderMemos[r].Addition2 + "</td><td>" + result.data.OrderMemos[r].Addition3 + "</td></tr>").appendTo($("#dOrderMemo"));
                }

                var t = $("#tdAttachment");
                t.html("");
                for (var r in result.data.Attachments) {
                    var a = $("<div title='点击下载'><img src='" + result.data.Attachments[r].Addition1 + "' alt=''/><a href='#' style='text-decoration:underline;' onclick=\"javascript:return DownLoadFile('" + result.data.Attachments[r].ItemValue + "');\">" + result.data.Attachments[r].ItemText + "</a>(" + result.data.Attachments[r].Addition2 + ")</div>");
                    a.appendTo(t);
                }

                $('#divDetail').dialog('open');
            }
            else {
                alert("获取信息失败.");
            }

        },
        error: function (e) {
            alert("获取信息失败.");
        }
    });
}

function AddPayment(id) {

    $.ajax({
        url: "/Order/GetPaymentDetail",
        type: 'POST',
        cache: false,
        data: { OrderNo: id },
        success: function (result) {
            if (result.code == 0) {             
                $("#Amount").html(result.data.Amount);
                $("#Paid").html(result.data.Paid);
                $("#PaymentHistory").html(result.data.PaymentHistory);

                if (result.data.Unpaid == 0) {
                    $("#unpaidDiv").hide();
                    $("#trAddPayment2").hide();
                    $("#trAddPayment3").hide();
                    $("#btnAddPayment").hide();
                }
                else {
                    $("#unpaidDiv").show();
                    $("#unpaidAmount").html(result.data.Unpaid);
                    $("#trAddPayment2").show();
                    $("#trAddPayment3").show();
                    $("#btnAddPayment").show();
                }

                $("#pdPaymentStatus").removeClass("OrderStatusUnPaid");
                $("#pdPaymentStatus").removeClass("OrderStatusPaid");
                $("#pdPaymentStatus").addClass(result.data.Css);

                $("#pdPaymentStatus").html(result.data.PaymentStatus);
                $("#PaymentAmount").val("");
                $("#PaymentType").val("");

                orderNo = result.data.OrderNo;
                $('#divPayment').dialog('open');
            }
            else {
                alert("获取信息失败.");
            }

        },
        error: function (e) {
            alert("获取信息失败.");
        }
    });

}

function SetColor() {
    $("#tOrder").find("tr").each(function (i, val) {
        if ($(val).find("td:eq(6)").find("span").html() == "未支付") {
            $(val).find("td:eq(6)").css("color", "#FF0000");
            $(val).addClass("orderUnpaid");
        }
        else {
            //$(val).find("td:eq(6)").css("color", "#00FF00");
        }

        if ($(val).find("td:eq(7)").find("span").html() == "未支付") {
            $(val).find("td:eq(7)").css("color", "#FF0000");
        }
        else {
            //$(val).find("td:eq(7)").css("color", "#00FF00");
        }
    });
}

function DeleteOrder(id) {
    if (!confirm("确定删除吗?")) {
        return;
    }

    $.ajax({
        url: "/Order/Delete",
        type: 'POST',
        cache: false,
        data: { OrderNumber: id },
        success: function (result) {
            if (result.code == 0) {
                $("#tOrder").find("tr[id='" + id + "']").remove();
            }
            else if (result.code == 1){
                alert(result.message);
            }

        },
        error: function (e) {
            alert('删除失败');
        }
    });
}

function EditOrder(id) {    
    var url = {};
    url.root = "/Order/Index";
    url.paras = [];
    if ($("#so").val() != "") {
        url.paras.push({ name: "so", value: $("#so").val() });
    }
    if ($("#ss").val() != "") {
        url.paras.push({ name: "ss", value: $("#ss").val() });
    }
    if ($("#sc").val() != "") {
    url.paras.push({ name: "sc", value: $("#sc").val() });
    }
    if ($("#sf").val() != "") {
        url.paras.push({ name: "sf", value: $("#sf").val() });
    }
    if ($("#sb").val() != "") {
        url.paras.push({ name: "sb", value: $("#sb").val() });
    }
    if ($("#se").val() != "") {
        url.paras.push({ name: "se", value: $("#se").val() });
    }
    
    url.paras.push({ name: "page", value: $($(".currentnum")[0]).html() });
   

    console.log(JSON.stringify(url)); 
    location.href = "/Order/Edit?OrderNo=" + id + "&GoBackURL=" + JSON.stringify(url);
}

function PaymentSubmit() {
   
    if (checkRequired('divPayment', 'errorMessagePayment') == false) {
        return;
    }
    if (parseInt($("#PaymentAmount").val()) == parseInt($("#unpaidAmount").html())) {
        if (!confirm("支付金额等于未付金额。\n订单状态将被设置为“已支付”。\n确定提交吗？")) {
            return;
        }
    }
    else if (parseInt($("#PaymentAmount").val()) < parseInt($("#unpaidAmount").html())) {
        if (!confirm("支付金额小于未付金额。\n订单状态将被设置为“部分支付”。\n确定提交吗？")) {
            return;
        }
    }
    else if (parseInt($("#PaymentAmount").val()) > parseInt($("#unpaidAmount").html())) {
        alert("支付金额不能大于未付金额。")
        return;
    }

    $.ajax({
        url: "/Order/PaymentAdd",
        type: 'POST',
        cache: false,
        dataType: 'json',
        data: { OrderNo: orderNo, PaymentAmount: $("#PaymentAmount").val(), PaymentType: $("#PaymentType").val(), PaymentMemo: $("#PaymentMemo").val() },
        success: function (result) {
            if (result.code == 0) {
                //$('#divPayment').dialog('close');
                location.href = "/Order";
            }
            else {
                $("#divPayment").find(".errorSpan").text(result.message);
            }
        },
        error: function (e) {
            $("#divPayment").find(".errorSpan").text('支付提交失败');
        }
    });
}

function DownLoadFile(no) {
    location.href = "/Order/DownLoadAttachment?FileNo=" + no;
}

function SearchCustomer() {
    $("#txtCustomer").val("");
    $('#divSearchCustomer').dialog('open');
}