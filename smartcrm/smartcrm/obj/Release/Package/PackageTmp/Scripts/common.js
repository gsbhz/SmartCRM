﻿function checkRequired(containerId,errorspanId) {
    $("#" + containerId).find("input").removeClass("redBorder");
    $("#" + containerId).find("select").removeClass("redBorder");
    $("#" + containerId).find("textarea").removeClass("redBorder");

    $("#" + errorspanId).text("");
   
    var checkResult = "";
    var checkStatus = true;

    $("#" + containerId).find("input[type='text'],input[type='password']").each(function (i, val) {
      
        if ($(val).parent().find(".redStar").length > 0) {
            if ($.trim($(val).val()) == "") {
                $(val).addClass("redBorder");

                checkStatus = false;
            }
        }

        if ($.trim($(val).val()).indexOf('<') >= 0
                     || $.trim($(val).val()).indexOf('>') >= 0
                     || $.trim($(val).val()).indexOf('"') >= 0
                     || $.trim($(val).val()).indexOf("'") >= 0) {
            $(val).addClass("redBorder");
            checkStatus = false;
            $("#" + errorspanId).text('输入包含非法字符： <，>，",\'');
        }
    });

    $("#" + containerId).find("select").each(function (i, val) {
        if ($(val).parent().find(".redStar").length > 0) {
            if ($.trim($(val).val()) == "") {
                $(val).addClass("redBorder");
                checkStatus = false;
            }
        }
    });

    $("#" + containerId).find("textarea").each(function (i, val) {
        if ($(val).parent().find(".redStar").length > 0) {
            if ($.trim($(val).val()) == "") {
                $(val).addClass("redBorder");
                checkStatus = false;
            }
        }

        if ($.trim($(val).val()).indexOf('<') >= 0
                     || $.trim($(val).val()).indexOf('>') >= 0
                     || $.trim($(val).val()).indexOf('"') >= 0
                     || $.trim($(val).val()).indexOf("'") >= 0) {
            $(val).addClass("redBorder");
            checkStatus = false;
            $("#" + errorspanId).text('输入包含非法字符： <，>，",\'');
        }
    });

    return checkStatus;
}

$(document).ready(function () {

    $(".main-navigation li").hover(function () {
        var itemwidth = $(this).width(); /* Getting the LI width */
        $(this).prepend("<div class='hover'></div>"); /* Inserting a blank div into within li above the <a> tag*/
        $(this).find("div").fadeIn('10000').css({ 'width': itemwidth }); /* Using the itemwidth for the div to display properly*/
        $(this).find("ul").fadeIn('1000').slideDown('10000').css("display", "block");

    }, function () {
        $(this).find("div").slideUp('1000').fadeOut('1000'); /* sliding up and fading out the hover div */
        $(this).find("div").remove(); /* removing the <div> code from html at every mouseout event*/
        $(this).find("ul").fadeOut('1000'); /* fading out the sub menu */

    });


});

function changeTwoDecimal(x) {
    var f_x = parseFloat(x);
    console.log(x);
    if (isNaN(f_x)) {        
        return x;
    }
    var f_x = Math.round(x * 100) / 100;
    return f_x;
}

function GenerateWorkFlowSteps(data) {
    var td = $("#tdWorkFlow");
    td.html("");
    var i = 0;
    var color = "#4BAFBE";
    for (i = 0; i < data.length; i++) {

        if (data[i].IsFinished) {
            color = "#4BAFBE";
        }
        else {
            color = "#FF0000";
        }
        var step = $("<div style='float:left; width:80px;height:100px;background-color:" + color + ";margin-right:0px;color:white'></div>");
        var div1 = $("<center style='font-weight:bold;color:yellow;margin-top:20px'>" + data[i].StepName + "</center>");
        var div2 = $("<center style='font-weight:bold;margin-top:5px'>" + data[i].UserName + "</center>");
        var div3 = $("<center style='margin-top:5px'>" + data[i].DateTime + "</center>");
        div1.appendTo(step);
        div2.appendTo(step);
        div3.appendTo(step);
        step.appendTo(td);
        if (i != data.length - 1) {
            var arrow = $("<img style='float:left; margin-top:40px;' src='/images/arrow.png' alt=''/>");
            arrow.appendTo(td);
        }
    }
}