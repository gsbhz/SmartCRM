﻿var FactoryNo = "";
var trId = "";
var isAdd = true;

function EditFactory(FactoryNo) {  
    isAdd = false;

    clearForm(isAdd);

    $.ajax({
        url: "/System/GetFactory",
        type: 'GET',
        cache: false,
        data: { FactoryNo: FactoryNo },
        success: function (result) {
            if (result.FactoryNo != "") {
                $("#FactoryName").val(result.FactoryName);
                $("#Description").val(result.Description);
                $("#FactoryNo").val(result.FactoryNo);
                $("#Contact").val(result.Contact);
                showFactory();
            }

        },
        error: function (e) {
        }
    });

    return false;
}

function DeleteFactory(FactoryNo) {
    if (!confirm('确定删除吗？')) {
        return;
    }
    $.ajax({
        url: "/System/DeleteFactory",
        type: 'POST',
        cache: false,
        data: { FactoryNo: FactoryNo },
        success: function (result) {
            if (result.code == 0) {
                location.href = '/System/Factory';
            }
            else {
                alert(result.message);
            }

        },
        error: function (e) {
            alert('删除失败');
        }
    });

    return false;
}

$(function () {

    $('#divFactory').dialog({
        width: 500,
        autoOpen: false,
        modal: true,
        buttons: {
        }
    });

    $('#btnNew').click(function () {
        isAdd = true;
        clearForm(isAdd);
        showFactory();
        $("#FactoryName").focus();
    });

    $('#btnCancel').click(function () {
        $('#divFactory').dialog('close');
    });


    $('#btnSave').click(function () {

        if (isAdd) {
            if (checkRequired('divFactory','errorMessage') == false) {
                return;
            }

            $.ajax({
                url: "/System/AddFactory",
                type: 'POST',
                cache: false,
                data: $('form').serialize(),
                success: function (result) {
                    if (result.code == 0) {
                        closeFactory();
                        location.href = "/System/Factory";
                    }
                    else {
                        $("#errorMessage").text(result.message);
                    }
                },
                error: function (e) {
                    $("#errorMessage").text("保存失败.");
                }
            });
        }
        else {
            if (!checkRequired('divFactory','errorMessage')) {
                return;
            }
            if (confirm('确定保存修改吗？')) {
                $.ajax({
                    url: "/System/UpdateFactory",
                    type: 'POST',
                    cache: false,
                    data: $('form').serialize(),
                    success: function (result) {
                        if (result.code == 0) {
                            closeFactory();
                            location.href = "/System/Factory";
                        }
                        else {
                            $("#errorMessage").text(result.message);
                        }
                    },
                    error: function (e) {
                        $("#errorMessage").text("保存失败.");
                    }
                });
            }
        }
    });
});

function showFactory() {
    $('#divFactory').dialog('open');
}

function closeFactory() {
    $('#divFactory').dialog('close');
}

function clearForm(isAdd) {
    $("input").removeClass("redBorder");
    $("input[type=text]").val("");
    $("textarea").val("");    
    $(".errorSpan").html("");
    if (isAdd) {       
        $("#FactoryName").focus();
    }
    else {
        $("#FactoryName").focus();
    }
}