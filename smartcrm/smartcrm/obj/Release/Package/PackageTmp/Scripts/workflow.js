﻿var WorkFlowNo = "";
var trId = "";
var isAdd = true;

function EditWorkFlow(WorkFlowNo) {  
    isAdd = false;

    clearForm(isAdd);

    $.ajax({
        url: "/System/GetWorkFlow",
        type: 'GET',
        cache: false,
        data: { WorkFlowNo: WorkFlowNo },
        success: function (result) {
            if (result.code == 0) {
                $("#WorkFlowNo").val(result.data.WorkFlowNo);
                $("#WorkFlowName").val(result.data.WorkFlowName);
                $("#WorkFlowDescription").val(result.data.WorkFlowDescription);
                if (result.data.Participate) {
                    $("#Participate").attr("checked", true);
                }
                else {
                    $("#Participate").attr("checked", false);
                }
                showWorkFlow();
                $("#WorkFlowName").focus();
            }
            else {
                $("#errorMessage").text(result.message);
            }

        },
        error: function (e) {
            $("#errorMessage").text("查找信息失败");
        }
    });

    return false;
}

function DeleteWorkFlow(WorkFlowNo) {
    if (!confirm('确定删除吗？')) {
        return;
    }
    $.ajax({
        url: "/System/DeleteWorkFlow",
        type: 'POST',
        cache: false,
        data: { WorkFlowNo: WorkFlowNo },
        success: function (result) {
            if (result.code == 0) {
                location.href = '/System/WorkFlow';
            }
            else {
                alert(result.message);
            }

        },
        error: function (e) {
            alert('删除失败');
        }
    });

    return false;
}


$(function () {

    $('#divWorkFlow').dialog({
        width: 500,
        autoOpen: false,
        modal: true,
        buttons: {
        }
    });

    $('#btnNew').click(function () {
        isAdd = true;
        clearForm(isAdd);
        showWorkFlow();        
    });

    $('#btnCancel').click(function () {
        $('#divWorkFlow').dialog('close');
    });


    $('#btnSave').click(function () {

        if (isAdd) {
            if (checkRequired('divWorkFlow', 'errorMessage') == false) {
                return;
            }

            $.ajax({
                url: "/System/AddWorkFlow",
                type: 'POST',
                cache: false,
                data: { WorkFlowNo: $("#WorkFlowNo").val(), WorkFlowName: $("#WorkFlowName").val(), WorkFlowDescription: $("#WorkFlowDescription").val() },
                success: function (result) {
                    if (result.code == 0) {
                        closeWorkFlow();
                        location.href = "/System/WorkFlow";
                    }
                    else {
                        $("#errorMessage").text(result.message);
                    }
                },
                error: function (e) {
                    $("#errorMessage").text("保存失败.");
                }
            });
        }
        else {
            if (!checkRequired('divWorkFlow', 'errorMessage')) {
                return;
            }
            if (confirm('确定保存修改吗？')) {              
                $.ajax({
                    url: "/System/UpdateWorkFlow",
                    type: 'POST',
                    cache: false,
                    data: { WorkFlowNo: $("#WorkFlowNo").val(), WorkFlowName: $("#WorkFlowName").val(), WorkFlowDescription: $("#WorkFlowDescription").val() },
                    success: function (result) {
                        if (result.code == 0) {
                            closeWorkFlow();
                            location.href = "/System/WorkFlow";
                        }
                        else {
                            $("#errorMessage").text(result.message);
                        }
                    },
                    error: function (e) {
                        $("#errorMessage").text("保存失败.");
                    }
                });
            }
        }
    });
});

function showWorkFlow() {
    $('#divWorkFlow').dialog('open');
}

function closeWorkFlow() {
    $('#divWorkFlow').dialog('close');
}

function clearForm(isAdd) {
    $("input").removeClass("redBorder");
    $("input[type=text]").val("");
    $("textarea").val("");  
    $(".errorSpan").html("");
    if (isAdd) {
        $("#WorkFlowNo").attr("disabled", false);
        $("#WorkFlowNo").focus();
        $("#WorkFlowNo").val($("#AutoNo").val());
    }
    else {
        $("#WorkFlowNo").attr("disabled", true);
        $("#WorkFlowName").focus();
    }
   
}