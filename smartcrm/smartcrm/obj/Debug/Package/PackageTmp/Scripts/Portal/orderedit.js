﻿var isAdd = true; // Status of order detail
var did = "";
var isOrderAdd;
var DetailArray = new Array(); // Store all order details
var orderTemplate = {};

$(function () {
    
    $("#Number").inputmask({ 'mask': "9{0,9}", greedy: false });

    $('#ContactNo').change(function () {
        if ($('#ContactNo').val() != "") {
            $("#Contact").val($("#ContactNo").find("option:selected").text());
        }
    });  

    $('#divOrderDetail').dialog({
        width: 640,
        autoOpen: false,
        modal: true,       
        buttons: {
        }
    });    

    $('#btnCancel').click(function () {
        if (confirm("确定要放弃修改吗？")) {
            location.href = "/Portal/Index";
        }
    });

    $('#btnDetailCancel').click(function () {
        $('#divOrderDetail').dialog('close');
    });

    $('#btnDetailClose').click(function () {
        $('#divOrderDetail').dialog('close');
    });

    $('#btnDetailSave').click(function () {
        if (checkRequired('divOrderDetail', 'errorMessageDetail') == false) {
            return;
        }  

        var newDetail;
        if (isAdd) {
            var date = new Date();
            did = "D" + date.getDate() + date.getHours() + date.getMinutes() + date.getSeconds() + date.getMilliseconds();

            newDetail = CreateNewDetailObject(did);
            DetailArray.push(newDetail);
            GenerateDetailTableRow(newDetail);
         
            $('#divOrderDetail').dialog('close');
            $('#tableEditDetail').attr("name", newDetail.did);
        }
        else {
            did = $('#tableEditDetail').attr("name");
            var tr = $('#tdOrderDetail').find("tr[id='" + did + "']");
            newDetail = CreateNewDetailObject(did);
            var orderDetailNo;
            for (var i = 0; i < DetailArray.length; i++) {
                if (DetailArray[i].did == did) {
                    DetailArray.splice(i, 1);
                    tr.find("td:eq(0)").text(newDetail.Content);
                    tr.find("td:eq(1)").text(newDetail.CategoryName);
                    tr.find("td:eq(2)").html(newDetail.Number);
                    break;
                }
            }
            DetailArray.push(newDetail);
            $('#divOrderDetail').dialog('close');

        }
    });

    $('#btnSave').click(function () {

        if (isOrderAdd) {
            if (checkRequired('divOrder', 'errorMessage') == false) {
                return;
            }
            if (DetailArray.length == 0) {
                $("#divOrder #errorMessage").text("必须包含至少一个明细项目");
                return;
            }

            if (!confirm("确定保存订单吗？")) {
                return;
            }

            $.ajax({
                url: "/Portal/CreateNewOrder",
                type: 'POST',
                cache: false,
                dataType: 'json',
                data: { BasicInfo: GetBasicInfo(), DetailInfo: GetDetailInfo() },
                success: function (result) {
                    if (result.code == 0) {
                        location.href = "/Portal/Index";
                    }
                    else {
                        $("#divOrder").find(".errorSpan").text(result.message);
                    }
                },
                error: function (e) {
                    $("#divOrder").find(".errorSpan").text('保存失败.');
                }
            });
        }
        else {
            if (!checkRequired('divOrder', 'errorMessage')) {
                return;
            }

            var additonMessage = "";

            if (confirm(additonMessage + '确定保存修改吗？')) {
                $.ajax({
                    url: "/Portal/UpdateOrder",
                    type: 'POST',
                    cache: false,
                    dataType: 'json',
                    data: { BasicInfo: GetBasicInfo(), DetailInfo: GetDetailInfo() },
                    success: function (result) {
                        if (result.code == 0) {
                            location.href = "/Portal/Index";
                        }
                        else {
                            $("#divOrder").find(".errorSpan").text(result.message);
                        }
                    },
                    error: function (e) {
                        $("#divOrder").find(".errorSpan").text('保存失败.');
                    }
                });
            }
        }
    });

   
    $("#ContactNo").val($("#hContactNo").val()); 
    orderTemplate = eval($("#OrderTemplate").val());
    for (var i in orderTemplate) {
        $("#Category").append("<option value=\"" + orderTemplate[i].id + "\">" + orderTemplate[i].text + "</option>");
    }

    $('#Category').change(function () {
        CategoryChange();
    });

    CategoryChange();

    if ($("#IsAdd").val() == "1") {
        isOrderAdd = true;
    }
    else {
        isOrderAdd = false;
        $("#aAddDetail").hide();

        // Retrieve order details
        $.ajax({
            url: "/Portal/GetOrderDetail",
            type: 'GET',
            cache: false,
            data: { OrderNo: $("#OrderNo").val() },
            success: function (result) {
                if (result.code == 0) {
                    CreateExistingDetailObject(result.data);
                    GenerateDetailTable();
                }
                else {
                    alert("获取订单信息失败");
                    window.history.back(-1);
                }
            },
            error: function (e) {
                alert("获取订单信息失败");
                window.history.back(-1);
            }
        });
    } 
});

function addDetail() {
    isAdd = true;
    did = "";
    clearForm();
    showOrderDetail();

    $("#divOrderDetail").find("input[type='text']").removeAttr("disabled");
    $("#divOrderDetail").find("textarea").removeAttr("disabled");
    $("#divOrderDetail").find("select").removeAttr("disabled");
    $("#btnDetailSave").show();
    $("#btnDetailCancel").show();
    $("#btnDetailClose").hide();
     
    return false;
}

function editDetail(id, readonly) {
    isAdd = false;
    did = id;
    clearForm();
    showOrderDetail();

    for (var i = 0; i < DetailArray.length; i++) {
        if (DetailArray[i].did == id) {
           
            $('#tableEditDetail').attr("name", DetailArray[i].did);
            $("#Content").val(DetailArray[i].Content);
            $("#Category").val(DetailArray[i].Category);
            $("#ReportCategory").val(DetailArray[i].ReportCategory);             
            $("#Number").val(DetailArray[i].Number);            
            $("#Memo").val(DetailArray[i].Memo);
                        
            $("#Category").change();
            SetDetailDetail(DetailArray[i].Detail);

            break;
        }
    }

    if (readonly) {
        $("#divOrderDetail").find("input[type='text']").attr("disabled", "disabled");
        $("#divOrderDetail").find("textarea").attr("disabled", "disabled");
        $("#divOrderDetail").find("select").attr("disabled", "disabled");
        $("#btnDetailSave").hide();
        $("#btnDetailCancel").hide();
        $("#btnDetailClose").show();
    }
    else {
        $("#divOrderDetail").find("input[type='text']").removeAttr("disabled");
        $("#divOrderDetail").find("textarea").removeAttr("disabled");
        $("#divOrderDetail").find("select").removeAttr("disabled");
        $("#btnDetailSave").show();
        $("#btnDetailCancel").show();
        $("#btnDetailClose").hide();        
    }
    
    return false;
}

function SetDetailDetail(obj) {

    ///eval("var objNew=" + obj + ";");
    var objNew = obj;
    for (var i in objNew) {      
        if ($("#"+ i ).length > 0){
            $("#" + i).val(objNew[i]);            
        } 
    }
 }

function showOrderDetail() {
    $('#divOrderDetail').dialog('open');
}

function closeOrder() {
    $('#divOrderDetail').dialog('close');
}

function clearForm() {
    $("#divOrderDetail").find("input").removeClass("redBorder");
    $("#divOrderDetail").find("select").removeClass("redBorder");
    $("#divOrderDetail").find("input[type=text]").val("");
    $("#divOrderDetail").find("select").val("");
    $("#divOrderDetail").find("textarea").val("");
    $("#divOrderDetail").find(".errorSpan").html("");
    $("#divOrderDetail").find("#Category").change();
} 

function GetBasicInfo(){
    var result = {};
    result.OrderNo = $("#OrderNo").val();
    result.OrderName = $("#OrderName").val();
    result.ContactNo = $("#ContactNo").val();
    result.Contact = $("#Contact").val(); 

    return JSON.stringify(result);
}

function GetDetailInfo() {
    for (var a in DetailArray) {
        if (DetailArray[a].Detail != null) {
            DetailArray[a].Detail = JSON.stringify(DetailArray[a].Detail);
        }
    }  
    return JSON.stringify(DetailArray);
}

function CreateNewDetailObject(did) {
    var newDetail = {
        did: did,       
        Content: $("#Content").val(),
        Category: $("#Category").val(),
        CategoryName: GetCategoryName($("#Category").val()),
        ReportCategory:$("#ReportCategory").val(),
        Detail: GetDetailDetail(),
        DetailText:GetDetailDetailText(),
        Number: $("#Number").val(),
        Memo: $("#Memo").val(),
        Price: 0,
        Amount: 0
    };

    newDetail.FactoryAmount = parseFloat(newDetail.FactoryAmount);
    newDetail.Amount = parseFloat(newDetail.Amount);

    return newDetail;
}

function GetDetailDetail() {
    var result = {};
    
    $("#tableDetail").find("input[id^='f']").each(function (i, t) {
        eval("result." + t.id + "='" + $(t).val() + "';");
    });
    $("#tableDetail").find("select[id^='f']").each(function (i, t) {
        eval("result." + t.id + "='" + $(t).val() + "';");
    });
  
    return result;
}

function GetDetailDetailText() {
    var result = "";
    $("#tableDetail").find("input[id^='f'],select[id^='f']").each(function (i, t) {
        result = result + $(t).parent().prev().html() + ": " + $(t).val() + ", ";
    });
    if (result.length > 2) {
        result = result.substr(0, result.length - 2);
    }
    return result;
}

function CreateExistingDetailObject(result) {

    for (var i in result) {
      
        var date = new Date();
        var did = "D" + date.getDate() + date.getHours() + date.getMinutes() + date.getSeconds() + date.getMilliseconds() ;
     
        var newDetail = {
            did: did,
            Content: result[i].Content,
            Category: result[i].Category,
            CategoryName: result[i].CategoryName,
            ReportCategory: result[i].ReportCategory,
            Detail: result[i].Detail,
            DetailText:result[i].DetailText,
            Number: result[i].Number,          
            Memo: result[i].Memo,
            Price: 0,
            Amount: 0
        };
        
        DetailArray.push(newDetail);
    }
}

function GenerateDetailTable() {  
    for (var i in DetailArray) {
        GenerateDetailTableRow(DetailArray[i]);
    }
}

function GetCategoryName(id) {     
    for (var i in orderTemplate) {
        
        if (orderTemplate[i].id == id) {
            return orderTemplate[i].text;
            break;
        }
    }
    return "";
}

function GenerateDetailTableRow(newDetail) {   
    var tr = document.createElement("tr");
    tr.id = newDetail.did;
    var td1 = document.createElement("td");
    $(td1).html(newDetail.Content);
    tr.appendChild(td1);

    var td2 = document.createElement("td");
    $(td2).html(GetCategoryName(newDetail.Category));  
    tr.appendChild(td2);

    var td3 = document.createElement("td");
    $(td3).html(newDetail.Number);
    tr.appendChild(td3);

    var td6 = $("<td></td>");

    var a1;
    if (isOrderAdd) {

        a1 = $("<img src='../../Images/grid/delete.png' title='删除' />");
        a1.click(function () {
            if (confirm("确定删除吗？")) {
                $('#tdOrderDetail').find("tr[id=" + newDetail.did + "]").remove();

                for (var i = 0; i < DetailArray.length; i++) {
                    if (DetailArray[i].did == newDetail.did) {
                        DetailArray.splice(i, 1);
                        CalculateAmount();
                        break;
                    }
                }

            }
        });
        a1.appendTo(td6);
    }

    a1 = $("<img src='../../Images/grid/edit.png' title='编辑' />");
    a1.click(function () {
        editDetail(newDetail.did, false);
    });

    a1.appendTo(td6);

    a1 = $("<img src='../../Images/grid/detail.png' title='查看详情' />");

    a1.click(function () {
        editDetail(newDetail.did, true);
    });
    a1.appendTo(td6);

    td6.appendTo(tr);

    document.getElementById("tdOrderDetail").appendChild(tr);
    $("#ReportCategory").val(newDetail.ReportCategory);
}

function CategoryChange() {
    var table = $("#tableDetail");
    table.find("tr").remove();
   
    var k = 0;
    var tr;
    var td;
    $("<tr class='title'><td colspan='4'>明细</td></tr>").appendTo(table);
    for (var i in orderTemplate) {
        if (orderTemplate[i].id == $('#Category').val()) {

            $("#ReportCategory").val(orderTemplate[i].reportCategory);
            for (var j in orderTemplate[i].fields) {
                k = k + 1;
                if (k % 2 == 1) {
                    tr = $("<tr></tr>")
                }

                td = $("<td class='label'>" + orderTemplate[i].fields[j].label + "</td>");

                td.appendTo(tr);

                td = $("<td></td>");
                var fieldId = "f" + orderTemplate[i].id + orderTemplate[i].fields[j].id;
                if (orderTemplate[i].fields[j].type == 1) {
                    // textbox
                    var textbox = $("<input type='text' id='" + fieldId + "' />");
                    textbox.appendTo(td);

                }
                else if (orderTemplate[i].fields[j].type == 2) {
                    // dropdown list
                    var ddl = $("<select id='" + fieldId + "'></select>");
                    for (var s in orderTemplate[i].fields[j].options) {
                        ddl.append("<option value=\"" + orderTemplate[i].fields[j].options[s] + "\">" + orderTemplate[i].fields[j].options[s] + "</option>");
                    }
                    ddl.appendTo(td);
                }
                else {
                    // TBD
                    // editable dropdownlist
                }
                if (orderTemplate[i].fields[j].isRequired) {
                    $("<span class='redStar'>*</span>").appendTo(td);
                }
                td.appendTo(tr);

                if (k % 2 == 0) {
                    tr.appendTo(table);
                }
            }

            if (k % 2 == 1) {
                td.attr("colspan", "3");
                tr.appendTo(table);
            }

            break;
        }
    }
}