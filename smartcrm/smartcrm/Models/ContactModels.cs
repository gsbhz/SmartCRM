﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web.Mvc;
using System.Web.Security;

namespace smartcrm.Models
{
    public class ContactList:PagerModel
    {
        public List<Contact> Contacts { get; set; }
        public string sCustomerNo { get; set; }
        public string Keywords { get; set; } 
        public List<SimpleItemList> DdlCustomer { get; set; }
        public string backButtonVisibility { get; set; }

        public List<String> Permissions { get; set; }       
    }
      
}
