﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web.Mvc;
using System.Web.Security;

namespace smartcrm.Models
{
    public class LoginUser:User
    {
        public List<string> PermissionList { get; set; }
        public LoginUser()
        {
            PermissionList = new List<string>();
        }
    }

}
