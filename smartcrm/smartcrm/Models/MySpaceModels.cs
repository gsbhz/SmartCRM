﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web.Mvc;
using System.Web.Security;

namespace smartcrm.Models
{
    public class MyOrders
    {
        public List<OrderInfo> InProcessingOrders { get; set; }
        public List<OrderInfo> InvolvedgOrders { get; set; }

        public string FileSizeLimitation { get; set; }
    }
}
