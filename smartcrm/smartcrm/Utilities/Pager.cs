﻿using System.Linq;
using System;
using Webdiyer.WebControls.Mvc;
using System.Collections.Generic;

public class PagedList<T> : List<T>, IPagedList
{
    public PagedList(IList<T> items, int pageIndex, int pageSize)//这是IList类型
    {
        PageSize = pageSize;
        TotalItemCount = items.Count;
        CurrentPageIndex = pageIndex;
        for (int i = StartRecordIndex - 1; i < EndRecordIndex; i++)
        {
            Add(items[i]);
        }
    }

    public PagedList(IEnumerable<T> items, int pageIndex, int pageSize, int totalItemCount)
    {
        AddRange(items);
        TotalItemCount = totalItemCount;
        CurrentPageIndex = pageIndex;
        PageSize = pageSize;
    }

    public int CurrentPageIndex { get; set; }
    public int PageSize { get; set; }
    public int TotalItemCount { get; set; }
    public int TotalPageCount { get { return (int)Math.Ceiling(TotalItemCount / (double)PageSize); } }
    public int StartRecordIndex { get { return (CurrentPageIndex - 1) * PageSize + 1; } }
    public int EndRecordIndex { get { return TotalItemCount > CurrentPageIndex * PageSize ? CurrentPageIndex * PageSize : TotalItemCount; } }
}

public static class PageLinqExtensions
{
    public static PagedList<T> ToPagedList<T>
        (
            this IEnumerable<T> allItems,
            int pageIndex,
            int pageSize
        )
    {
        if (pageIndex < 1)
            pageIndex = 1;      
        
        var itemIndex = (pageIndex - 1) * pageSize;
        var pageOfItems = allItems.Skip(itemIndex).Take(pageSize);
        var totalItemCount = allItems.Count();
        return new PagedList<T>(pageOfItems, pageIndex, pageSize, totalItemCount);
    }
}