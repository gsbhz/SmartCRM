﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Reflection;
using System.ComponentModel;
using smartcrm.Models;


namespace smartcrm.Utilities
{
    public class EnumHelper
    {
        public static List<SimpleItemList> GetDdlList(Type enumType, bool WithBlank)
        {
            List<SimpleItemList> result;
            result = new List<SimpleItemList>();
            if (WithBlank)
            {
                result.Add(new SimpleItemList() { ItemText = string.Empty, ItemValue = string.Empty });
            }

            FieldInfo[] allFields = enumType.GetFields();
            object[] attrInfo = null;
            string descData = "";
            foreach (FieldInfo e in allFields)
            {
                if (enumType.FullName == e.FieldType.FullName)
                {
                    attrInfo = e.GetCustomAttributes(typeof(DescriptionAttribute), true);
                    if (attrInfo != null && attrInfo.Length > 0)
                    {
                        descData = (attrInfo[0] as DescriptionAttribute).Description;
                        result.Add(new SimpleItemList(){ ItemValue = e.Name,ItemText=descData});
                    }
                    else
                    {
                        result.Add(new SimpleItemList() { ItemValue = e.Name, ItemText = e.Name });
                    }
                }
            }

            return result;
        }

        public static List<SimpleItemList> GetDdlListWithIntId(Type enumType, bool WithBlank)
        {
            List<SimpleItemList> result;
            result = new List<SimpleItemList>();
            if (WithBlank)
            {
                result.Add(new SimpleItemList() { ItemText = string.Empty, ItemValue = string.Empty });
            }

            FieldInfo[] allFields = enumType.GetFields();
            object[] attrInfo = null;
            string descData = "";
            foreach (FieldInfo e in allFields)
            {
                if (enumType.FullName == e.FieldType.FullName)
                {
                    attrInfo = e.GetCustomAttributes(typeof(DescriptionAttribute), true);
                    if (attrInfo != null && attrInfo.Length > 0)
                    {
                        descData = (attrInfo[0] as DescriptionAttribute).Description;
                        result.Add(new SimpleItemList() { ItemValue = ((int)(e.GetValue(e))).ToString(), ItemText = descData });
                    }
                    else
                    {
                        result.Add(new SimpleItemList() { ItemValue = ((int)(e.GetValue(e))).ToString(), ItemText = e.Name });
                    }
                }
            }

            return result;
        }

        public static void GetDdlListWithInt(Type enumType, Dictionary<string, string> dic)
        {

            FieldInfo[] allFields = enumType.GetFields();
            object[] attrInfo = null;
            string descData = "";
            foreach (FieldInfo e in allFields)
            {
                if (enumType.FullName == e.FieldType.FullName)
                {
                    attrInfo = e.GetCustomAttributes(typeof(DescriptionAttribute), true);
                    descData = (attrInfo[0] as DescriptionAttribute).Description;
                   
                    if (attrInfo != null && attrInfo.Length > 0)
                    {
                        dic.Add(((int)(e.GetValue(e))).ToString(), descData);
                    }
                    else
                    {
                        dic.Add(((int)(e.GetValue(e))).ToString(), e.Name);
                    }
                }
            }
        }

        public static string GetEnumDescriptionByValue(Type enumType, string value)
        {
            FieldInfo[] allFields = enumType.GetFields();
            Enum v = Enum.Parse(enumType, value) as Enum;
            FieldInfo fi = v.GetType().GetField(v.ToString());
            DescriptionAttribute[] attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);
            if (attributes.Length > 0)
            {
                return attributes[0].Description;
            }
            else
            {
                return v.ToString();
            }
        }
    }
}