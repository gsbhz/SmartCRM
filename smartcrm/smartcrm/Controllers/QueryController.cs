﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using smartcrm.Models;
using smartcrm.Utilities;

namespace smartcrm.Controllers
{
    public class QueryController : BaseController
    {
        public ActionResult Order(string id)
        {            
            string errorMessage = string.Empty;
            OrderQueryResult model = new OrderQueryResult();
            if (id.Length > 4)
            {
                string querySN = id.Substring(id.Length - 4);
                string orderNo = id.Substring(0, id.Length - 4);
                Order order = Context.Order.FirstOrDefault(m => m.QueryCode == querySN && m.OrderNo == orderNo);
                
                if (orderNo != null)
                {
                    //model.CustomerName = order.Customer.CustomerShortName;
                    model.TransactionDate = order.TransactionDate;
                    model.OrderDetailText = order.DetailText;
                    //model.Number = order.Number;
                    model.OrderName = order.OrderName;
                    model.OrderNo = order.OrderNo;
                    model.Status = order.OrderStatus.StatusName;
                    model.CompanyName = order.Company.CompanyNameOnReport;
                    model.Phone = order.Company.PhoneFaxOnReport;
                    List<OrderFlow> flows = Context.OrderFlow.Where(m => m.OrderNo == orderNo).OrderBy(m => m.OrderFlowNo).ToList();

                    foreach (OrderFlow of in flows)
                    {
                        SimpleItemList step = new SimpleItemList();
                        if (of.IsFinished)
                        {
                            step.Addition1 = of.EndDate.Value.ToString("yyyy-MM-dd HH:mm:ss");
                            step.ItemText = "已完成";
                        }
                        else
                        {
                            step.Addition1 = of.BeginDate.Value.ToString("yyyy-MM-dd HH:mm:ss");
                            step.ItemText = "进行中";
                        }
                        step.Addition2 = of.WorkFlowStep.WorkFlowStepName;
                        step.Addition3 = of.UserName;

                        model.WorkFlows.Add(step);
                    }
                }
                else
                {
                    model.Message = "订单不存在";
                }

            }
            else
            {
                model.Message = "订单不存在";
            }
            return View(model);
        }

    }
}
